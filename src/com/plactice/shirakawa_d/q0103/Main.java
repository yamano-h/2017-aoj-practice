package com.plactice.shirakawa_d.q0103;

import java.io.InputStreamReader;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;

class Main {

    public static void main(String[] args) throws Exception {
        try (Scanner sc = new Scanner(new InputStreamReader(System.in))) {
            int inningCount = sc.nextInt();

            Queue<String> actions = new ConcurrentLinkedQueue<>();
            while (sc.hasNext()) {
                actions.add(sc.next());
            }

            for (int i = 0; i < inningCount; i++) {
                System.out.println(processInning(actions));
            }
        }
    }

    private static int processInning(Queue<String> actions) {
        Inning inning = new Inning();

        while (!inning.isGameOver()) {
            String action = actions.poll();

            switch (action) {
            case "HIT":
                inning.hit();
                break;
            case "OUT":
                inning.out();
                break;
            case "HOMERUN":
                inning.homerun();
                break;
            }
        }

        return inning.getPoints();
    }

    static enum Base {
        RUNNER,
        NO_RUNNER;

        public boolean isRunner() {
            return (this == RUNNER);
        }
    }

    static class Inning {
        private final Queue<Base> ground;
        private int outs = 0;
        private int points = 0;

        public Inning() {
            ground = new ConcurrentLinkedQueue<>();
            ground.add(Base.NO_RUNNER);// first
            ground.add(Base.NO_RUNNER);// second
            ground.add(Base.NO_RUNNER);// third
        }

        public void hit() {
            points += postFirst(Base.RUNNER);
        }

        public void homerun() {
            for (int i = 0; i < ground.size(); i++) {
                points += postFirst(Base.NO_RUNNER);
            }
            points++; // batter's point
        }

        public void out() {
            outs++;
        }

        /**
         * add RUNNER or NO_RUNNER to first base
         * 
         * @return 1 if third had RUNNER, 0 if not.
         */
        private int postFirst(Base base) {
            ground.add(base);

            Base third = ground.poll();
            return third.isRunner() ? 1 : 0;
        }

        public boolean isGameOver() {
            return 3 <= outs;
        }

        public int getPoints() {
            return points;
        }
    }

}
