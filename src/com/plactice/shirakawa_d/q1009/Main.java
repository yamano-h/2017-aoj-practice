package com.plactice.shirakawa_d.q1009;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Objects;

class Main {
    /**
     * 入力の読み取り / 回答の出力を担う
     */
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while (!isEndLine(line = br.readLine())) {
            String answer = processLine(line);
            System.out.println(answer);
        }
    }

    /**
     * 終了条件を判断する
     */
    private static boolean isEndLine(String line) {
        return Objects.isNull(line);
    }

    /**
     * 入出力データと、ビジネスロジックが利用可能なデータとの変換を行う
     */
    private static String processLine(String line) {
        String[] strs = line.split(" ");
        long a = Long.parseLong(strs[0]);
        long b = Long.parseLong(strs[1]);

        return String.valueOf(calcGdc(a, b));
    }

    /**
     * ビジネスロジックにあたる、最大公約数を求めるメソッド
     */
    private static long calcGdc(long a, long b) {
        // ユークリッドのアルゴリズムを利用している

        while (b > 0) {
            long tmp = a % b;
            a = b;
            b = tmp;
        }

        return a;
    }
}
