package com.plactice.shirakawa_d.q0025;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Objects;

class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String lineA;
        String lineB;
        while (!isEndLine(lineA = br.readLine())) {
            lineB = br.readLine();
            System.out.println(processLine(lineA, lineB));
        }
    }

    private static boolean isEndLine(String line) {
        return Objects.isNull(line);
    }

    private static String processLine(String lineA, String lineB) {
        String[] a = lineA.split(" ");
        String[] b = lineB.split(" ");

        return String.format("%d %d", checkHit(a, b), checkBrow(a, b));
    }

    private static int checkHit(String[] a, String[] b) {
        int count = 0;

        for (int i = 0; i < a.length; i++) {
            // 同じ位置で同じ文字なら、ヒットとして数える
            if (a[i].equals(b[i])) {
                count++;
            }
        }

        return count;
    }

    private static int checkBrow(String[] a, String[] b) {
        // このロジックは、入力の中に同じ数字が含まれている場合を考慮していない。
        // (たとえば 1 2 2 3 のような)

        int count = 0;

        for (int ai = 0; ai < a.length; ai++) {
            for (int bi = 0; bi < b.length; bi++) {
                // 同じ位置のものは比較対象外
                if (ai == bi) {
                    continue;
                }

                // 異なる位置で同じ文字が含まれていたら、ブローとして数える
                if (b[bi].equals(a[ai])) {
                    count++;
                    break;
                }
            }
        }

        return count;
    }

}
