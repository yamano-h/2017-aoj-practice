package com.plactice.shirakawa_d.ITP1_3_B;

import java.io.*;

class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line;
        int count = 1;

        for (;;) {
            line = br.readLine();
            if (line.equals("0")) {
                break;
            }

            System.out.println(String.format("Case %d: %s", count, line));
            count++;
        }
    }
}
