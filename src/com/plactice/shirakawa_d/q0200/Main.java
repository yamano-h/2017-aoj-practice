package com.plactice.shirakawa_d.q0200;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

class Main {

    public static void main(String[] args) throws Exception {
        try (Scanner sc = new Scanner(new InputStreamReader(System.in))) {
            RouteMap routeMap = RouteMap.create(sc);

            int questionNum = sc.nextInt();

            for (int i = 0; i < questionNum; i++) {
                int startStation = sc.nextInt();
                int goalStation = sc.nextInt();
                int calcMode = sc.nextInt();

                int answer = calcCost(routeMap, startStation, goalStation, calcMode);
                System.out.println(answer);
            }

        }
    }

    private static int calcCost(RouteMap routeMap, int startStation, int goalStation, int calcMode) {
        // stationまでの最低costを保持
        int[] lowerCost = new int[routeMap.stationCount()];
        Arrays.fill(lowerCost, Integer.MAX_VALUE);
        lowerCost[startStation - 1] = 0; // startは0

        calcStation(routeMap, lowerCost, calcMode, startStation);

        return lowerCost[goalStation - 1];
    }

    private static void calcStation(RouteMap routeMap, int[] lowerCost, int calcMode, int target) {
        List<RouteEdge> edges = routeMap.getEdges(target);

        // target と隣り合った各駅のコストを見て・・・
        for (RouteEdge edge : edges) {
            int otherside = edge.getOthersideStation(target);
            int cost = lowerCost[target - 1] + edge.getValue(calcMode);

            // otherside までの最低コストが更新された場合、otherside 以降を再探索
            if (cost < lowerCost[otherside - 1]) {
                lowerCost[otherside - 1] = cost;
                calcStation(routeMap, lowerCost, calcMode, otherside);
            }
        }
    }

    static class RouteMap {
        private final Map<Integer, List<RouteEdge>> edgeMap;

        public RouteMap(Map<Integer, List<RouteEdge>> edgeMap) {
            this.edgeMap = edgeMap;
        }

        public List<RouteEdge> getEdges(int station) {
            return edgeMap.get(Integer.valueOf(station));
        }

        public static RouteMap create(Scanner sc) {
            int edgeCount = sc.nextInt();
            int stationCount = sc.nextInt();

            Map<Integer, List<RouteEdge>> edgeMap = new HashMap<>();

            for (int i = 1; i <= stationCount; i++) {
                edgeMap.put(Integer.valueOf(i), new ArrayList<>());
            }

            for (int i = 0; i < edgeCount; i++) {
                RouteEdge edge = RouteEdge.create(sc);

                Integer stationA = Integer.valueOf(edge.getStationA());
                edgeMap.get(stationA).add(edge);

                Integer stationB = Integer.valueOf(edge.getStationB());
                edgeMap.get(stationB).add(edge);
            }

            return new RouteMap(edgeMap);
        }

        int stationCount() {
            return edgeMap.size();
        }
    }

    static class RouteEdge {
        private final int stationA;
        private final int stationB;
        private final int[] values; // 0=cost, 1=time

        private RouteEdge(int stationA, int stationB, int cost, int time) {
            super();
            this.stationA = stationA;
            this.stationB = stationB;
            this.values = new int[2];
            this.values[0] = cost;
            this.values[1] = time;
        }

        public static RouteEdge create(Scanner sc) {
            return new RouteEdge(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt());
        }

        public int getStationA() {
            return stationA;
        }

        public int getStationB() {
            return stationB;
        }

        public int getOthersideStation(int station) {
            return (stationA == station) ? stationB : stationA;
        }

        public int getValue(int calcMode) {
            return values[calcMode];
        }
    }

}
