package com.plactice.shirakawa_d.q0006;

import java.io.BufferedReader;
import java.io.InputStreamReader;

class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line = br.readLine();
        System.out.println(processLine(line));
    }

    private static String processLine(String line) {
        StringBuilder sb = new StringBuilder(line);
        return sb.reverse().toString();
    }
}
