package com.plactice.shirakawa_d.q0027;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Calendar;

class Main {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        String line;
        while (!isEndLine(line = br.readLine())) {
            System.out.println(processLine(line));
        }
    }

    private static boolean isEndLine(String line) {
        return line.equals("0 0");
    }

    private static String processLine(String line) {
        String[] strs = line.split(" ");
        int month = Integer.parseInt(strs[0]);
        int date = Integer.parseInt(strs[1]);

        // 2004年の日付しか入力されない
        return String.valueOf(calcDayOfWeek(2004, month, date));
    }

    public static final String[] DAYS = {
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday" };

    private static String calcDayOfWeek(int year, int month, int date) {
        Calendar cal = Calendar.getInstance();

        // Calendarでは1月が0になるので、month-1している
        cal.set(year, month - 1, date);

        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

        return DAYS[dayOfWeek - 1];
    }
}
